import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { IconButton, Grid, Typography } from '@material-ui/core';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Skeleton from  '@material-ui/lab/Skeleton';
import axios from 'axios';
import { ProductsToolbar, ProductCard } from './components';
import mockData from './data';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3)
  },
  content: {
    marginTop: theme.spacing(6)
  },
  pagination: {
    marginTop: theme.spacing(3),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
}));
var products=[];
const ProductList = () => {
  const classes = useStyles();
  const [products1,setproducts] = useState(<React.Fragment>
       <Grid
    container
    spacing={3}
  >
  <Grid
    item
  // style={{display:'block'}}
    lg={4}
    md={6}
    xs={12}
  >
    <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
<div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
    <Skeleton  animation="wave" variant="rect" width={400} height={40} />

  </Grid>
  <Grid
  item

  lg={4}
  md={6}
  xs={12}
>
    <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
<div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
    <Skeleton  animation="wave" variant="rect" width={400} height={40} />
  </Grid>
  <Grid
  item

  lg={4}
  md={6}
  xs={12}
>
    <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
<div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
    <Skeleton  animation="wave" variant="rect" width={400} height={40} />
  </Grid>
  <Grid
  item

  lg={4}
  md={6}
  xs={12}
>
    <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
<div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
    <Skeleton  animation="wave" variant="rect" width={400} height={40} />
  </Grid>
  <Grid
  item

  lg={4}
  md={6}
  xs={12}
>
    <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
<div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
    <Skeleton  animation="wave" variant="rect" width={400} height={40} />
  </Grid>
  <Grid
  item

  lg={4}
  md={6}
  xs={12}
>
    <Skeleton  animation="wave" variant="rect" width={400} height={170} style={{marginBottom:'30'}} /> 
<div style={{marginBottom:'30',display:"hidden"}} ><br/> </div>
    <Skeleton  animation="wave" variant="rect" width={400} height={40} />
  </Grid>

</Grid>
</React.Fragment>);

useEffect(()=>{
  document.title="Fitsen / Therapists";
  axios.get('https://fitsen.herokuapp.com/getTheris').then(res=>{

products=Object.values(res.data);
setproducts(         <Grid
container
spacing={3}
>
{products[0].map(product => (

  <Grid
    item
    key={product._id}
    lg={4}
    md={6}
    xs={12}
  >
<ProductCard product={product} />
  </Grid>
))}
</Grid>)



})})
return(
<div className={classes.root}>
{/* <ProductsToolbar /> */}
<div className={classes.content}>

{products1}

</div>

</div>


);
};

export default ProductList;
