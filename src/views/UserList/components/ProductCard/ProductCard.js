import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import TimelapseOutlinedIcon from '@material-ui/icons/TimelapseOutlined';


import Rating from '@material-ui/lab/Rating';
import {
  Card,
  CardContent,
  CardActions,
  Typography,
  Grid,
  Divider
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import GetAppIcon from '@material-ui/icons/GetApp';
import StarHalfIcon from '@material-ui/icons/StarHalf';
import PeopleOutlineIcon from '@material-ui/icons/PeopleOutline';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import Popover from '@material-ui/core/Popover';




const useStyles = makeStyles(theme => ({
  root: {},
  typography: {
    padding: theme.spacing(1),
  },
  popover: {
    pointerEvents: 'none',
  },
  paper: {
    padding: theme.spacing(1),
  },
  imageContainer: {
    height: 64,
    width: 64,
    margin: '0 auto',
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: '5px',
    overflow: 'hidden',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '100%'
  },
  statsItem: {
    display: 'flex',
    alignItems: 'center'
  },
  statsIcon: {
    color: theme.palette.icon,
    marginRight: theme.spacing(1)
  }
}));

const ProductCard = props => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handlePopoverOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const { className, product, ...rest } = props;

  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
       <CardContent>
        <div className={classes.imageContainer}>
          <img
            alt="Product"
            className={classes.image}
            src={product.Avatar}
          />
        </div>
        <Typography
          align="center"
          gutterBottom
          variant="h4"
        >
        {product.firstname} {product.lastname}
        </Typography>
        {/* <Typography
          align="center"
          variant="body1"
        >
          {product.email}
        </Typography> */}
        <div style={{ marginLeft:"47%",width:"100%",justifyContent:"center"}}> 
         <EmailOutlinedIcon        aria-owns={open ? 'mouse-over-popover' : undefined}
        aria-haspopup="true"
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}/>
        <Popover
        id="mouse-over-popover"
        className={classes.popover}
        classes={{
          paper: classes.paper,
        }}
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        <Typography>{product.email}</Typography>
      </Popover></div>
      </CardContent>
      <Divider />
      <CardActions>
        <ExpansionPanel>
          <ExpansionPanelSummary>
        <Grid
          container
          justify="space-between"
        >
          <Grid
            className={classes.statsItem}
            item
          >
    <Rating style={{position:'center'}}name="read-only size-medium" precision={0.5} defaultValue={4.5}  readOnly  size="small" />
            {/* <Typography
              display="inline"
              variant="body2"
            >
               5/5 Rating
            </Typography> */}
          </Grid>
          <Grid
            className={classes.statsItem}
            item
          >
            <TimelapseOutlinedIcon  className={classes.statsIcon}/>
            <Typography
              display="inline"
              variant="body2"
            >
       {product.experience} years 
                  </Typography>
          </Grid>
        </Grid></ExpansionPanelSummary>
        <ExpansionPanelDetails>
    
        <Typography>
        {product.about}
          </Typography> 
        </ExpansionPanelDetails>
        </ExpansionPanel>
      </CardActions>
    </Card>
  );
};

ProductCard.propTypes = {
  className: PropTypes.string,
  product: PropTypes.object.isRequired
};

export default ProductCard;
