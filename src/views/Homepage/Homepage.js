import React,{Component} from 'react';
import { makeStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AccessibilityOutlinedIcon from '@material-ui/icons/AccessibilityOutlined';
import FitnessCenterOutlinedIcon from '@material-ui/icons/FitnessCenterOutlined';
import PeopleOutlineOutlinedIcon from '@material-ui/icons/PeopleOutlineOutlined';
import FavoriteBorderOutlinedIcon from '@material-ui/icons/FavoriteBorderOutlined';




const useStyles = makeStyles(theme => ({
    root: {
       
      padding: theme.spacing(5),
      height:"90vh",
      backgroundImage:`url(${'https://i.imgur.com/PwGqNIQ.jpg'})`,
      backgroundSize:'cover'
    },
    inner:{
        padding: theme.spacing(5)     
    },
   root1: {
flexGrow:1,
marginTop:theme.spacing(7) 
    },
    inner:{
// display:"flex"
    },
    content: {
      marginTop: theme.spacing(6)
    },
    card:{
        minHeight:"700"
    },  title: {
        marginTop: theme.spacing(3)
      },
    pagination: {
      marginTop: theme.spacing(3),
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end'
    }
  }));
 const Homepage = props =>{
    const classes = useStyles();
    const handleClick1=()=>{
        props.history.push('/sign-in');
    }
    const handleClick2=()=>{
        props.history.push('/sign-up');
    }
     return(
         <React.Fragment>
             <div className={classes.root}>
                <div className={classes.inner}>
                <Typography
                  className={classes.title}
                  variant="h1"
                  align='center' 
                  lg={4}
                  style={{width:"100%",color:"Black", fontWeight:'bolder',paddingBottom:"10%"}}
                > <img
                alt="Logo"
                src="/images/logos/fitsen-logo1.png" /></Typography>
<Grid  container className={classes.root1} style={{position:"absolute",width:'50vw',top:"25vh",left:'25vw'}}spacing={2}>
    
    <Grid item lg={5} style={{marginRight:"50"}}> 
 <div style={{border:"3px solid transparent",borderRadius:"10px",display:'block',justifyContent:"center"}} onClick={handleClick1}>
     <PeopleOutlineOutlinedIcon style={{fontSize:100,marginLeft:"34%",marginTop:"10%"}}/>
     <Typography
                  className={classes.title}
                  variant="h1"
                  align='center' 
                  lg={4}
                  style={{width:"100%", fontFamily:"Montserrat",color:"Black",paddingBottom:"10%"}}
                >
                  Sign in as User
                </Typography>

 </div>
    </Grid>    
    <Grid item lg={2}></Grid>
    <Grid item lg={5}> 
 <div style={{border:"3px solid transparent",borderRadius:"10px",display:'block',justifyContent:"center"}} onClick={handleClick2}>
     <FavoriteBorderOutlinedIcon  style={{fontSize:100,marginLeft:"32%",marginTop:"10%"}}/>
     <Typography
                  className={classes.title}
                  variant="h2"
                  align='center' 
                  lg={4}
                  style={{width:"100%",color:"Black",fontFamily:"Montserrat",paddingBottom:"10%"}}
                >
                  Sign in as Health Coach
                </Typography>

 </div>
    </Grid>    
    </Grid>
                </div>
             </div>
         </React.Fragment>
     )
 }

 export default Homepage;