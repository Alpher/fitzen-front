import React,{Component} from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import jwtDecode from 'jwt-decode';
import axios from 'axios';
import {
  Budget,
  TotalUsers,
  TasksProgress,
  TotalProfit,
  LatestSales,
  UsersByDevice,
  LatestProducts,
  LatestOrders
} from './components';

const classes ={
  root: {
    padding:4
  }
};

class Dashboard extends Component{
componentDidMount(){
  document.title="Fitsen / Dashboard";
  if(!localStorage.getItem('usertoken')){
    this.props.history.push('/home');
  }
    let x=jwtDecode(localStorage.getItem('usertoken'));
    axios.post("",{"id":x._id}).then(res=>{

    })
   

  
}
render(){
return (
    <div className={classes.root}>
      <Grid
        container
        spacing={3}
        style={{marginTop:5,marginLeft:5,maxWidth:'83vw'}}
      >
        <Grid
          item
          lg={3}
          sm={6}
          xl={3}
          xs={12}
        >
          <Budget weight={{}}/>
        </Grid>
        <Grid
          item
          lg={3}
          sm={6}
          xl={3}
          xs={12}
        >
          <TotalUsers />
        </Grid>
        <Grid
          item
          lg={3}
          sm={6}
          xl={3}
          xs={12}
        >
          <TasksProgress />
        </Grid>
        <Grid
          item
          lg={3}
          sm={6}
          xl={3}
          xs={12}
        >
          <TotalProfit />
        </Grid>
        <Grid
          item
          lg={8}
          md={12}
          xl={9}
          xs={12}
        >
          <LatestSales />
        </Grid>
        <Grid
          item
          lg={4}
          md={6}
          xl={3}
          xs={12}
        >
          <UsersByDevice />
        </Grid>
        {/* <Grid
          item
          lg={4}
          md={6}
          xl={3}
          xs={12}
        >
          <LatestProducts />
        </Grid>
        <Grid
          item
          lg={8}
          md={12}
          xl={9}
          xs={12}
        >
          <LatestOrders />
        </Grid>
       */}
      </Grid>
    </div>
  );}
};

export default Dashboard;
