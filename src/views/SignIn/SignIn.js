import React, { useState, useEffect } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { makeStyles } from '@material-ui/styles';
import createBrowserHistory from 'history/createBrowserHistory';


import { login } from '../../UserFunctions';
import {
  Grid,
  Button,
  IconButton,
  TextField,
  Link,
  Typography
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import { Facebook as FacebookIcon, Google as GoogleIcon } from 'icons';

// const schema = {
//   email: {
//     presence: { allowEmpty: false, message: 'is required' },
//     email: true,
//     length: {
//       maximum: 64
//     }
//   },
//   password: {
//     presence: { allowEmpty: false, message: 'is required' },
//     length: {
//       maximum: 128
//     }
//   }
// };

const useStyles = makeStyles(theme => ({
  root:{
    display:'flex',
    backgroundColor:"#943E66",
    height:"100vh"
  },
  root1: {
    backgroundColor: 'white',
    height: '100vh',
    width:'80vw',
    left:'0',
    position:'absolute',
    backgroundImage:`url(${'https://i.pinimg.com/originals/85/1b/a9/851ba92c0d61565f3a3b1487ad89d504.gif'})`,
    backgroundSize:'100% 100%'
  },
  root2:{
left:'80vw',
position:"absolute",width:"19vw",
background:'transparent',

height:""
  },
  grid: {
    height: '100%'
  },
  quoteContainer: {
    [theme.breakpoints.down('md')]: {
      display: 'none'
    }
  },
  quote: {
    backgroundColor: theme.palette.neutral,
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundImage: 'url(/images/auth.jpg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center'
  },
  quoteInner: {
    textAlign: 'center',
    flexBasis: '600px'
  },
  quoteText: {
    color: theme.palette.white,
    fontWeight: 300
  },
  name: {
    marginTop: theme.spacing(3),
    color: theme.palette.white
  },
  bio: {
    color: theme.palette.white
  },
  contentContainer: {},
  content: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  contentHeader: {
    display: 'flex',
    alignItems: 'center',
    paddingTop: theme.spacing(5),
    paddingBototm: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },
  logoImage: {
    marginLeft: theme.spacing(4)
  },
  contentBody: {
   height:"40vh",
    display: 'flex',
    
    [theme.breakpoints.down('md')]: {
      
    }
  },
  form: {
    // paddingLeft: 100,
    // paddingRight: 100,
    paddingBottom: 125,
    flexBasis: 700,
    [theme.breakpoints.down('sm')]: {
      // paddingLeft: theme.spacing(2),
      // paddingRight: theme.spacing(2)
    }
  },
  title: {
    marginTop: theme.spacing(3)
  },
  socialButtons: {
    marginTop: theme.spacing(3)
  },
  socialIcon: {
    marginRight: theme.spacing(1)
  },
  sugestion: {
    marginTop: theme.spacing(2)
  },
  textField: {
    marginTop: theme.spacing(2)
  },
  signInButton: {
    margin: theme.spacing(2, 0)
  }
}));

const SignIn = props => {
  const { history } = props;
 const tokencheck='';
  const classes = useStyles();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  // useEffect(() => {
  //   const errors = validate(formState.values, schema);

  //   setFormState(formState => ({
  //     ...formState,
  //     isValid: errors ? false : true,
  //     errors: errors || {}
  //   }));
  // }, [formState.values]);

  const handleBack = () => {
    history.goBack();
  };

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleSignIn = event => {

    event.preventDefault();
  
    window.location.replace('https://fitsen.herokuapp.com/auth/google');
  
// setTimeout(function(){ history.push('/users');},2000)
   
  };

  // const hasError = field =>
  //   formState.touched[field] && formState.errors[field] ? true : false;


   
    if(localStorage.getItem('usertoken')){
      console.log("Already Exists");
   history.push('/user/dashboard'); 
   return (null)
    }
    else{
  return (
<div className={classes.root}>
<div className={classes.root1}></div>
    <div className={classes.root2}>
      <Grid
        className={classes.grid}
        container 

      >
        <Grid className={classes.content}
        item
        lg={12}
       
        />
        {/* <Grid
          className={classes.quoteContainer}
          item
          lg={5}
        >
          <div className={classes.quote}>
            <div className={classes.quoteInner}>
              <Typography
                className={classes.quoteText}
                variant="h1"
              >
                Hella narwhal Cosby sweater McSweeney's, salvia kitsch before
                they sold out High Life.
              </Typography>
              <div className={classes.person}>
                <Typography
                  className={classes.name}
                  variant="body1"
                >
                  Takamaru Ayako
                </Typography>
                <Typography
                  className={classes.bio}
                  variant="body2"
                >
                  Manager at inVision
                </Typography>
              </div>
            </div>
          </div>
        </Grid>
        */}
        <Grid
          className={classes.content}
          item
          lg={12}
        
        ><div style={{width:"50"}}>
             <img
            alt="Logo"
            src="/images/logos/fitsen-logo1.png" 
         style={{height:100 ,position:'absolute',top:90,left:-100}}
          /> </div>
            {/* <div className={classes.contentHeader}>
              <IconButton onClick={handleBack}>
                <ArrowBackIcon />
              </IconButton>
            </div> */}
            {/* <div className={classes.contentBody} style={{height:"100"}}
            >

                <Typography
                  className={classes.title}
                  variant="h1"
                  align='bottom'
                  lg={4}
                  style={{width:"100%",color:"white",fontWeight:'bolder',paddingTop:"70%"}}
                >
                  Log in
                </Typography></div> */}
                {/* <Typography
                  color="textSecondary"
                  gutterBottom
                >
                  Sign in with social media
                </Typography>
                 */}

<div className={classes.contentBody } style={{display:'block'}}
            >
                <Grid
                  className={classes.socialButtons}
                  container
                  spacing={2}
                  style={{justifyContent:'left',left:-80,top:300,position:'absolute'}}
                  
                >
                  {/* <Grid className={classes.content}
                  container
                  lg={5} > */}
                    {/* <Button
                      color="primary"
                      onClick={handleSignIn}
                      size="large"
                      variant="contained"
                    >
                      <FacebookIcon className={classes.socialIcon} />
                      Login with Facebook
                    </Button> */}
                  {/* </Grid> */}
                  <Grid item >
                   
                    <Button
                      onClick={handleSignIn}
                      size="large"
                      variant="contained"
                      style={{ color:"Red",fontSize:"larger"}}
                      
                      
                    >
                      <GoogleIcon className={classes.socialIcon}   />
                      Login with Google
                    </Button>
                  </Grid>
                
                
                </Grid>
                {/* <Typography
                  align="center"
                  className={classes.sugestion}
                  color="textSecondary"
                  variant="body1"
                >
                  or login with email address
                </Typography>
                <TextField
                  className={classes.textField}
                  error={hasError('email')}
                  fullWidth
                  helperText={
                    hasError('email') ? formState.errors.email[0] : null
                  }
                  label="Email address"
                  name="email"
                  onChange={handleChange}
                  type="text"
                  value={formState.values.email || ''}
                  variant="outlined"
                />
                <TextField
                  className={classes.textField}
                  error={hasError('password')}
                  fullWidth
                  helperText={
                    hasError('password') ? formState.errors.password[0] : null
                  }
                  label="Password"
                  name="password"
                  onChange={handleChange}
                  type="password"
                  value={formState.values.password || ''}
                  variant="outlined"
                />
                <Button
                  className={classes.signInButton}
                  color="primary"
                  disabled={!formState.isValid}
                  fullWidth
                  size="large"
                  type="submit"
                  variant="contained"
                >
                  Sign in now
                </Button> */}
                {/* <Typography
                  color="textSecondary"
                  variant="body1"
                >
                  Don't have an account?{' '}
                  <Link
                    component={RouterLink}
                    to="/sign-up"
                    variant="h6"
                  >
                    Sign up
                  </Link>
                </Typography> */}
         
            </div>
          
        </Grid>
      </Grid>
    </div>
   
</div> );}
};

SignIn.propTypes = {
  history: PropTypes.object
};

export default withRouter(SignIn);
