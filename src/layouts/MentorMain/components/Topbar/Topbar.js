import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { AppBar, Toolbar, Badge, Hidden, IconButton, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
import createBrowserHistory from 'history/createBrowserHistory';
const history = createBrowserHistory();
const useStyles = makeStyles(theme => ({
  logo:{
   fontFamily:"Lato",
   textDecoration:"none",
   fontSize:"20px",
   color:"white",
   fontWeight:"bolder"
  },
  root: {
    boxShadow: 'none',
     
   
  },
  flexGrow: {
    flexGrow: 1
  },
  signOutButton: {
    marginLeft: theme.spacing(1)
  }
}));

const Topbar = props => {

  const { className, onSidebarOpen, ...rest } = props;

  function logOut () {
   
    localStorage.removeItem('usertoken')
   history.push('/');
  }


  const classes = useStyles();

  const [notifications] = useState([]);

  return (
    <AppBar
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Toolbar>
        <RouterLink to="/" style={{textDecoration:'none'}}>
        <Typography className={classes.logo}>
          <img
            alt="Logo"
            src="/images/logos/fitsen-logo3.png"
            width="30%"
          /> 
          {/* Fitsen */}
          </Typography>
        </RouterLink>
        <div className={classes.flexGrow} />
        <Hidden mdDown>
          <IconButton color="inherit">
            <Badge
              badgeContent={notifications.length}
              color="primary"
              variant="dot"
            >
              <NotificationsIcon />
            </Badge>
          </IconButton>
          {/* <IconButton
            className={classes.signOutButton}
            color="inherit"
            onClick={logOut}
          >
            <InputIcon />
          </IconButton> */}
        </Hidden>
        <Hidden lgUp>
          <IconButton
            color="inherit"
            onClick={onSidebarOpen}
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

Topbar.propTypes = {
  className: PropTypes.string,
  onSidebarOpen: PropTypes.func
};

export default Topbar;
