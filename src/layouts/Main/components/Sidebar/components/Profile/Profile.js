import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content'
  },
  avatar: {
    width: 60,
    height: 60
  },
  name: {
    marginTop: theme.spacing(1)
  }
}));

const Profile = props => {
  const { className,user, ...rest } = props;

  const classes = useStyles();

  const user1 = {
    name: 'Shen Zhi',
    avatar: '/images/avatars/avatar_11.png',
    bio: 'Brain Director'
  };

  return (
    <div style={{paddingTop:30,paddingBottom:20}}>
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <Avatar
        alt="Person"
        className={classes.avatar}
        // component={RouterLink}
        src={user.user.avatar}
        to="/user/dashboard"
      />
      <Typography
        className={classes.name}
        variant="h4"
      >
        {user.user.name}
      </Typography>
      <Typography variant="body2">{user.user.email}</Typography>
    </div>
    </div> );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
